# The Haskell/0 Report

NOTE:
+ The `splncs04.bst` and `llncs.cls` files are simply TeX styles of
  Springer's LNCS; they are not part of the report.
+ The report is currently available in Chinese only.
+ The report is still currently a draft.
