module Main where

import Tokenizer
import Parser
import Transformation
import Core
import LambdaLifting
import G
import HZPrelude
import System.Environment

main :: IO ()
main = do {
    args <- getArgs;
    interact (choose (args !! 0))
}

fromRight (Right x) = x

toTokenize = tokenize
toParse = parseString
toParseUnwrap = unwrapProgram . toParse
toTransform = transformProgram . toParseUnwrap
toLambdaLift = lambdaLift . toTransform

choose "--tokenize" = show . toTokenize
choose "--parse" = show . toParse
choose "--parseunwrap" = show . toParseUnwrap
choose "--transform" = show . toTransform
choose "--lambdalift" = show . toLambdaLift
choose "--compile" = showState . compile . toLambdaLift
choose "--eval" = getOutput . last . eval . compile . toLambdaLift
choose "--evalwithtrail" = showResults . eval . compile . toLambdaLift
