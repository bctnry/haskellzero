module Tokenizer (
    TokenType(..)
,   Token
,   Length
,   tokenize
,   getTokenStr
,   nextTokenPos
,   removeComment
) where
import Auxillary.Function
import Text.Parsec.Pos
import Text.ParserCombinators.Parsec hiding (newline)
import Data.Maybe (fromJust)
import HZPrelude

data TokenType = If | Case | Of | Let | Letrec | Data | In
               | LParen | RParen | LBrace | RBrace | Semicolon | Slash | RightArrow
               | Whitespace String | Newline String | Comment String
               | Varid String | Constid String | Wildcard
               | MidfixOp String | SymbolOp String
               | FloatLiteral String | IntLiteral String | CharLiteral String
               deriving (Eq, Show)
type Token = (TokenType, Int, Int)
type Length = Int

getTokenStr :: Token -> String
getTokenStr (x, _, _) = case x of
    If -> "if"
    Case -> "case"
    Of -> "of"
    Let -> "let"
    Letrec -> "letrec"
    In -> "in"
    Data -> "data"
    LParen -> "("
    RParen -> ")"
    LBrace -> "{"
    RBrace -> "}"
    Semicolon -> ";"
    Slash -> "\\"
    RightArrow -> "->"
    (Whitespace x) -> x
    (Newline x) -> x
    (Comment x) -> x
    Wildcard -> "_"
    (Varid x) -> x
    (Constid x) -> x
    (MidfixOp x) -> x
    (SymbolOp x) -> x
    (FloatLiteral x) -> x
    (IntLiteral x) -> x
    (CharLiteral x) -> x

nextTokenPos :: SourcePos -> Token -> [Token] -> SourcePos
nextTokenPos sp (t, line, col) toks = case t of
    If -> newPos (sourceName sp) line (col + 2)
    Case -> newPos (sourceName sp) line (col + 4)
    Of -> newPos (sourceName sp) line (col + 2)
    Data -> newPos (sourceName sp) line (col + 4)
    Let -> newPos (sourceName sp) line (col + 3)
    Letrec -> newPos (sourceName sp) line (col + 6)
    In -> newPos (sourceName sp) line (col + 2)
    LParen -> newPos (sourceName sp) line (col + 1)
    RParen -> newPos (sourceName sp) line (col + 1)
    LBrace -> newPos (sourceName sp) line (col + 1)
    RBrace -> newPos (sourceName sp) line (col + 1)
    Semicolon -> newPos (sourceName sp) line (col + 1)
    Slash -> newPos (sourceName sp) line (col + 1)
    RightArrow -> newPos (sourceName sp) line (col + 2)
    (Whitespace x) -> newPos (sourceName sp) line (col + length x)
    (Newline x) -> newPos (sourceName sp) (line + 1) 1
    (Comment x) -> newPos (sourceName sp) (line + 1) 1
    Wildcard -> newPos (sourceName sp) line (col + 1)
    (Varid x) -> newPos (sourceName sp) line (col + length x)
    (Constid x) -> newPos (sourceName sp) line (col + length x)
    (MidfixOp x) -> newPos (sourceName sp) line (col + length x)
    (SymbolOp x) -> newPos (sourceName sp) line (col + length x)
    (FloatLiteral x) -> newPos (sourceName sp) line (col + length x)
    (IntLiteral x) -> newPos (sourceName sp) line (col + length x)
    (CharLiteral x) -> newPos (sourceName sp) line (col + length x)


tokenizerWrapper :: Parser TokenType -> Parser Token
tokenizerWrapper x = try $ do
    sourcepos <- getPosition
    res <- x
    return (res, sourceLine sourcepos, sourceColumn sourcepos)

discard :: Parser a -> Parser ()
discard x = x >> return ()

lparen :: Parser Token
lparen = tokenizerWrapper $ (char '(' >> return LParen)
rparen :: Parser Token
rparen = tokenizerWrapper $ (char ')' >> return RParen)
lbrace :: Parser Token
lbrace = tokenizerWrapper $ (char '{' >> return LBrace)
rbrace :: Parser Token
rbrace = tokenizerWrapper $ (char '}' >> return RBrace)
semicolon :: Parser Token
semicolon = tokenizerWrapper $ (char ';' >> return Semicolon)
slash :: Parser Token
slash = tokenizerWrapper $ ((char '\\' >> return Slash) <?> "slash (\"\\\")")
whitespace :: Parser Token
whitespace = tokenizerWrapper $ ((many1 (oneOf " \t") >>= \x-> return (Whitespace x)) <?> "whitespace")
newline :: Parser Token
newline = tokenizerWrapper $ (((string "\n" <|> string "\r" <|> string "\r\n") >>= return . Newline ) <?> "newline")
comment :: Parser Token
comment = tokenizerWrapper $ (((string "--" >> many (noneOf "\n\r") >>= \s-> (discard newline <|> eof) >> return (Comment ("--" ++ s)))) <?> "beginning of comment (\"--\")")
idchar :: Parser Char
idchar = alphaNum <|> char '\'' <|> char '_'
varid_ :: Parser String
varid_ = do
    h <- lower
    r <- many idchar
    return (h:r)
varid :: Parser Token
varid = tokenizerWrapper $ do
    x <- varid_
    return $ f x
    where f x = case x of {
        "data" -> Data; "if" -> If; "let" -> Let; "letrec" -> Letrec;
        "case" -> Case; "of" -> Of; "in" -> In;
        _ -> Varid x
    }
constid_ :: Parser String
constid_ = do
    h <- upper
    r <- many idchar
    return (h:r)
constid :: Parser Token
constid = tokenizerWrapper $ constid_ >>= return . Constid
wildcard :: Parser Token
wildcard = tokenizerWrapper $ (string "_" >> return Wildcard)
midfixop :: Parser Token
midfixop = tokenizerWrapper $ do
    char '`'
    x <- varid_
    char '`'
    return $ MidfixOp x
opsymbol :: Parser Char
opsymbol = oneOf "!#$&%?^~*+/<=>-:.|"
symbolop :: Parser Token
symbolop = tokenizerWrapper $ do
    x <- many1 opsymbol
    return $ f x
    where f "->" = RightArrow
          f x = SymbolOp x
numliteral :: Parser Token
numliteral = tokenizerWrapper $ do
    x <- many1 digit
    y <- optionMaybe (char '.' >> many1 digit)
    return $ f x y
    where f x Nothing = IntLiteral x
          f x (Just y) = FloatLiteral (x ++ '.':y)
graphic :: Parser Char
graphic = alphaNum <|> oneOf "!#$&%?^~*+/<=>.@\\|(),;[]_{}\"`:-"
charliteral :: Parser Token
charliteral = tokenizerWrapper $ do
    x <- between (char '\'') (char '\'') (charesc <|> count 1 graphic)
    return $ CharLiteral (('\'':x)++"\'")
    where charesc = do
              string "\\x" <?> "hexadecimal escape code (\"slash x\")"
              x <- count 2 hexDigit <?> "exactly 2 hex digit"
              return ('\\':'x':x)
          

tokenize_ :: Parser Token
tokenize_ = choice [
    whitespace, newline, comment,
    varid, constid, wildcard,
    midfixop, symbolop,
    lparen, rparen, lbrace, rbrace, semicolon, slash,
    numliteral, charliteral]

tokenize :: String -> [Token]
tokenize str = case parse myparse "" str of {
    t@(Left x) -> error $ show x;
    Right (res, nextpos, rest) ->
        if (null rest)
        then [res]
        else res : tokenize rest
} where
    myparse = do {
        tokens <- tokenize_;
        pos <- getPosition;
        rest <- getInput;
        return (tokens, pos, rest)
    }

removeComment :: [Token] -> [Token]
removeComment x = filter (\(x, _, _) -> case x of { Comment _ -> False; _ -> True }) x