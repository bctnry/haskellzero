module Utils where
import Data.Maybe as Maybe
import qualified Data.Map as Map
import qualified Data.Set as Set

-- heap.
hInitial :: Heap a
hAlloc :: Heap a -> a -> (Heap a, Addr)
hUpdate :: Heap a -> Addr -> a -> Heap a
hFree :: Heap a -> Addr -> Heap a

hLookup :: Heap a -> Addr -> a
hAddresses :: Heap a -> [Addr]
hSize :: Heap a -> Int

hNull :: Addr
hIsnull :: Addr -> Bool

type Heap a = (Int, [Int], Map.Map Int a)
type Addr = Int
hInitial = (0, [1..], Map.empty)
hAlloc (size, (next:free), cts) n = ((size + 1, free, Map.insert next n cts), next)
hUpdate (size, free, cts) a n = (size, free, Map.insert a n cts)
hFree (size, free, cts) a = (size - 1, a:free, Map.delete a cts)
hLookup (size, free, cts) a = case Map.lookup a cts of
    Nothing -> error $ "Failed to find #" ++ show a ++ " in heap."
    Just x -> x
hAddresses (size, free, cts) = Map.keys cts
hSize (size, _, _) = size
hNull = 0
hIsnull = (== hNull)

type ASSOC a b = Map.Map a b
aLookup a k d = case a Map.!? k of
    Nothing -> d
    Just x -> x
aDomain = Map.keys
aRange = Map.elems
aEmpty = Map.empty
aContainKey k a = Maybe.isJust $ Map.lookupIndex k a
aInsert k v a = Map.insert k v a

aMapvalue f m = Map.map f m
aFromList x = Map.fromList x
aUnion a1 a2 = Map.union a1 a2
aSize x = Map.size x

type NameSupply = Int
initialNameSupply :: NameSupply
initialNameSupply = 0
getName :: NameSupply -> String -> (NameSupply, String)
getName ns prefix = (ns + 1, makeName prefix ns)
getNames :: NameSupply -> [String] -> (NameSupply, [String])
getNames ns prefixes = (ns + length prefixes, zipWith makeName prefixes [ns..])
makeName prefix ns = prefix ++ "_" ++ show ns
