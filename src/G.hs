module G where
import Auxillary.Function
import Core
import HZPrelude
import Parser
import Transformation
import LambdaLifting
import Utils
import qualified Data.Map as Map
import Data.Maybe
import Data.List (mapAccumL)
import Data.Char (chr, ord)

-- auxillary functions.
showCon (CECon t a) = "$" ++ show t ++ "," ++ show a
readCon x = (t, a) where
    pairstr = tail x
    t = read $ takeWhile isDigit pairstr
    a = read $ tail $ dropWhile isDigit pairstr
    isDigit x = x `elem` "0123456789"

-- G Machine 定义。
type GState = (GOutput, GCode, GStack, GDump, GHeap, GGlobals, GStats)

type GOutput = String
getOutput :: GState -> GOutput
getOutput (o, _, _, _, _, _, _) = o
putOutput :: GOutput -> GState -> GState
putOutput o (_, i, st, d, h, g, sts) = (o, i, st, d, h, g, sts)

type GCode = [GInstruction]
getCode :: GState -> GCode
getCode (_, c, _, _, _, _, _) = c
putCode :: GCode -> GState -> GState
putCode c (o, _, st, d, h, g, stats) = (o, c, st, d, h, g, stats)

type GStack = [Addr]
getStack :: GState -> GStack
getStack (_, _, st, _, _, _, _) = st
putStack :: GStack -> GState -> GState
putStack st (o, c, _, d, h, g, stats) = (o, c, st, d, h, g, stats)

type GDump = [GDumpItem]
type GDumpItem = (GCode, GStack)
getDump :: GState -> GDump
getDump (_, _, _, d, _, _, _) = d
putDump :: GDump -> GState -> GState
putDump d (o, i, st, _, h, g, sts) = (o, i, st, d, h, g, sts)

type GHeap = Heap Node
getHeap :: GState -> GHeap
getHeap (_, _, _, _, h, _, _) = h
putHeap :: GHeap -> GState -> GState
putHeap h (o, c, st, d, _, g, stats) = (o, c, st, d, h, g, stats)

type GGlobals = ASSOC CName Addr
getGlobals :: GState -> GGlobals
getGlobals (_, _, _, _, _, g, _) = g
putGlobals :: GGlobals -> GState -> GState
putGlobals g (o, c, st, d, h, _, stats) = (o, c, st, d, h, g, stats)

statInitial :: GStats
statIncSteps :: GStats -> GStats
statGetSteps :: GStats -> Int
type GStats = Int
statInitial = 0
statIncSteps = (+1)
statGetSteps = id
getStats :: GState -> GStats
getStats (_, _, _, _, _, _, stats) = stats
putStats :: GStats -> GState -> GState
putStats stats (o, c, st, d, h, g, _) = (o, c, st, d, h, g, stats)


-- GInstructions.
data GInstruction
    = Unwind
    | Pushglobal CName
    | Pushint Int | Pushfloat Double | Pushchar Char
    | Push Int
    | Mkap
    | Slide Int
    | Update Int | Pop Int
    | Alloc Int
    | Eval
    | Addint | Subint | Mulint | Divint | Negint
    | Addfloat | Subfloat | Mulfloat | Divfloat | Negfloat
    | Eqint | Neint | Ltint | Leint | Gtint | Geint
    | Eqfloat | Nefloat | Ltfloat | Lefloat | Gtfloat | Gefloat
    | Eqchar | Nechar | Ltchar | Lechar | Gtchar | Gechar
    | Asfloat
    | Chr | Ord
    | Floor | Ceiling | Round
    | Eq | Ne | Lt | Le | Gt | Ge
    | Cond GCode GCode
    | Pack Int Int
    | Casejump (ASSOC Int GCode)
    | Split Int
    | Print
    deriving (Eq, Show)


-- data definitions.
data Node
    = NInt Int | NFloat Double | NChar Char
    | NApp Addr Addr
    | NGlobal Int GCode
    | NInd Addr
    | NConstr Int [Addr]
    deriving (Eq, Show)

showNode heap (NInt x) = show x
showNode heap (NFloat x) = show x
showNode heap (NChar x) = show x
showNode heap (NApp x y) = (showNode heap (hLookup heap x)) ++ " " ++ (showNode heap (hLookup heap y))
showNode heap (NGlobal x y) = "<Thunk>"
showNode heap (NInd x) = showNode heap (hLookup heap x)
showNode heap (NConstr x y)
    | x == 0 = "Unit"
    | x == 1 = "False"
    | x == 2 = "True"
    | x == 3 = "MkTuple{" ++ showNode heap (hLookup heap (y !! 0)) ++ "," ++ showNode heap (hLookup heap (y !! 1)) ++ "}"
    | x == 4 = "Nil"
    | x == 5 = "Cons{" ++ interleave "," (map (showNode heap . hLookup heap) y) ++ "}"
    | x == 6 = "Nothing"
    | x == 7 = "Just{" ++ showNode heap (hLookup heap (head y)) ++ "}"
    | otherwise = "Constr_" ++ show x ++ "{" ++ interleave "," (map (showNode heap . hLookup heap) y) ++ "}"

eval :: GState -> [GState]
eval state = state:resStates where
    resStates
        | gFinal state = []
        | otherwise = eval nextState
    nextState = doAdmin (step state)
doAdmin :: GState -> GState
doAdmin s = putStats (statIncSteps $ getStats s) s
gFinal :: GState -> Bool
gFinal = null . getCode
step :: GState -> GState
step state = dispatch i (putCode is state) where
    (i:is) = getCode state
walk :: Int -> GState -> GState
walk 0 s = s
walk n s = walk (n - 1) (step s)

dispatch :: GInstruction -> GState -> GState
dispatch (Pushglobal f) = pushglobal f
dispatch (Pushint n) = pushint n
dispatch (Pushfloat n) = pushfloat n
dispatch (Pushchar n) = pushchar n
dispatch Mkap = mkap
dispatch (Push n) = push n
dispatch (Slide n) = slide n
dispatch Unwind = unwind
dispatch (Update n) = update n
dispatch (Pop n) = pop n
dispatch (Alloc n) = alloc n
dispatch (Addint) = intarithmetic2 (+)
dispatch (Subint) = intarithmetic2 (-)
dispatch (Mulint) = intarithmetic2 (*)
dispatch (Divint) = intarithmetic2 (div)
dispatch (Negint) = intarithmetic1 (negate :: Int -> Int)
dispatch (Addfloat) = floatarithmetic2 (+)
dispatch (Subfloat) = floatarithmetic2 (-)
dispatch (Mulfloat) = floatarithmetic2 (*)
dispatch (Divfloat) = floatarithmetic2 (/)
dispatch (Negfloat) = floatarithmetic1 (0-)
dispatch Eqint = intcomparison (==)
dispatch Neint = intcomparison (/=)
dispatch Ltint = intcomparison (<)
dispatch Leint = intcomparison (<=)
dispatch Gtint = intcomparison (>)
dispatch Geint = intcomparison (>=)
dispatch Eqfloat = floatcomparison (==)
dispatch Nefloat = floatcomparison (/=)
dispatch Ltfloat = floatcomparison (<)
dispatch Lefloat = floatcomparison (<=)
dispatch Gtfloat = floatcomparison (>)
dispatch Gefloat = floatcomparison (>=)
dispatch Eqchar = charcomparison (==)
dispatch Nechar = charcomparison (/=)
dispatch Ltchar = charcomparison (<)
dispatch Lechar = charcomparison (<=)
dispatch Gtchar = charcomparison (>)
dispatch Gechar = charcomparison (>=)
dispatch Asfloat = primitive1 boxFloat unboxInteger fromIntegral
dispatch Chr = primitive1 boxChar unboxInteger chr
dispatch Ord = primitive1 boxInteger unboxChar ord
dispatch Eval = insEval
dispatch (Cond i1 i2) = cond i1 i2
dispatch (Pack t n) = pack t n
dispatch (Casejump branches) = casejump branches
dispatch (Split n) = split n
dispatch Print = insPrint
dispatch Floor = primitive1 boxInteger unboxFloat floor
dispatch Ceiling = primitive1 boxInteger unboxFloat ceiling
dispatch Round = primitive1 boxInteger unboxFloat round

pushglobal :: CName -> GState -> GState
pushglobal f state
    | registered = putStack (aLookup (getGlobals state) f (error "This case cannot theoretically happen.") : getStack state) $ state
    | otherwise = putStack newStack $ putHeap newHeap $ putGlobals newGlobals $ state
    where registered = aContainKey f (getGlobals state)
          (t, n) = readCon f
          (newHeap, a) = hAlloc (getHeap state) (NGlobal n [Pack t n, Update 0, Unwind])
          newStack = a : getStack state
          newGlobals = aInsert f a (getGlobals state)

pushLiteral :: (Show a) => (a -> Node) -> a -> GState -> GState
pushLiteral wrapper value state
    | aContainKey (show value) (getGlobals state)
        =   let a = aLookup (getGlobals state) (show value) (error "This case cannot theoretically happen")
                newStack = a : (getStack state)
            in putStack newStack $ state
    | otherwise
        =   let (newHeap, a) = hAlloc (getHeap state) (wrapper value)
                newStack = a : getStack state
                newGlobals = aInsert (show value) a (getGlobals state)
            in putGlobals newGlobals $ putHeap newHeap $ putStack newStack $ state

pushint :: Int -> GState -> GState
pushint = pushLiteral NInt
pushfloat :: Double -> GState -> GState
pushfloat = pushLiteral NFloat
pushchar :: Char -> GState -> GState
pushchar = pushLiteral NChar

mkap :: GState -> GState
mkap state = putHeap newHeap $ putStack newStack $ state where
    (newHeap, a) = hAlloc (getHeap state) (NApp a1 a2)
    (a1:a2:as') = getStack state
    newStack = a:as'

push :: Int -> GState -> GState
push n state = putStack newStack state where
    oldStack = getStack state
    newStack = (oldStack !! n) : oldStack

getArg :: Node -> Addr
getArg (NApp _ arg) = arg

slide :: Int -> GState -> GState
slide n state = putStack (a : drop n as) state where
    (a : as) = getStack state

unwind :: GState -> GState
unwind state = newState (hLookup heap a) where
    (a : as) = getStack state
    heap = getHeap state
    newState (NInt n) = handleWhnf state
    newState (NFloat n) = handleWhnf state
    newState (NChar n) = handleWhnf state
    newState (NConstr n as) = handleWhnf state
    newState (NApp a1 a2) = putCode [Unwind] (putStack (a1:a:as) state)
    newState (NGlobal n c)
        | length as < n
            = let ((i, s) : d_) = getDump state
                  ak = last (getStack state)
              in putCode i $ putStack (ak : s) $ putDump d_ $ state
              -- TODO: this might not work.
        | otherwise
            = let rearranged = rearrange n (getHeap state) (getStack state)
              in putCode c $ putStack rearranged $ state
    newState (NInd a) = putCode [Unwind] $ putStack newStack $ state where
        (a0 : s) = getStack state
        newStack = (a : s)
    handleWhnf state = case getDump state of
        [] -> state
        (i', s') : d -> putCode i' $ putStack (a : s') $ putDump d $ state



rearrange :: Int -> GHeap -> GStack -> GStack
rearrange n heap as = take n as' ++ drop n as where
    as' = map (getArg . hLookup heap) (tail as)


update :: Int -> GState -> GState
update n state = putHeap newHeap $ putStack newStack $ state where
    (a : as) = getStack state
    newStack = as
    newHeap = hUpdate (getHeap state) (as !! n) (NInd a)

pop :: Int -> GState -> GState
pop n state = putStack newStack state where
    newStack = drop n (getStack state)

alloc :: Int -> GState -> GState
alloc n state = putHeap newHeap $ putStack newStack $ state where
    (newHeap, addedAddr) = allocNodes n (getHeap state)
    newStack = addedAddr ++ (getStack state)
allocNodes :: Int -> GHeap -> (GHeap, [Addr])
allocNodes 0 heap = (heap, [])
allocNodes n heap = (heap2, a : as) where
    (heap1, as) = allocNodes (n - 1) heap
    (heap2, a) = hAlloc heap1 (NInd hNull)

insEval :: GState -> GState
insEval state = putCode [Unwind] $ putStack newStack $ putDump newDump $ state where
    (a : s) = getStack state
    newStack = [a]
    newDump = (getCode state, s) : getDump state

cond :: GCode -> GCode -> GState -> GState
cond i1 i2 state = putCode newCode $ putStack newStack $ state where
    (a : newStack) = getStack state
    decision = hLookup (getHeap state) a
    newCode = (if decision == NConstr 2 [] then i1 else i2) ++ getCode state

pack :: Int -> Int -> GState -> GState
pack t n state = putStack newStack $ putHeap newHeap $ state where
    ans = take n (getStack state)
    (newHeap, a) = hAlloc (getHeap state) (NConstr t ans)
    newStack = a : (drop n $ getStack state)

casejump :: ASSOC Int GCode -> GState -> GState
casejump lst state = putCode newCode $ state where
    (a : _) = getStack state
    NConstr t _ = hLookup (getHeap state) a
    newCode = (aLookup lst t (error $ "Failed to find branch for <" ++ show t ++ ">")) ++ getCode state

split :: Int -> GState -> GState
split _ state
    = let (a : s) = getStack state
      in case hLookup (getHeap state) a of
             NConstr t ans -> putStack (ans ++ s) $ state
             _ -> error "Failed to split on a non-constructor."

insPrint :: GState -> GState
insPrint state = putOutput newOutput $ putStack newStack $ state where
    (a : newStack) = getStack state
    newOutput = getOutput state ++ showNode (getHeap state) (hLookup (getHeap state) a)

-- the compiler.

compile :: CoreProgram -> GState
compile program = ("", initialCode, [], [], heap, globals, statInitial) where
    (heap, globals) = buildInitialHeap (parsedPrelude ++ program) -- !
    parsedPrelude = lambdaLift $ transformProgram $ Parser.unwrapProgram $ Parser.parseString $ HZPrelude.preludeFunctions
buildInitialHeap :: CoreProgram -> (GHeap, GGlobals)
buildInitialHeap program = built where
    (h, listassoc) = mapAccumL allocateSc hInitial compiled
    built = (h, aFromList listassoc)
    compiled = map compileSc (program) ++ compiledPrimitives


type GCompiledSC = (CName, Int, GCode)

compiledPrimitives :: [GCompiledSC]
compiledPrimitives = [
    ("+", 2, [Push 1, Eval, Push 1, Eval, Addint, Update 2, Pop 2, Unwind]),
    ("-", 2, [Push 1, Eval, Push 1, Eval, Subint, Update 2, Pop 2, Unwind]),
    ("*", 2, [Push 1, Eval, Push 1, Eval, Mulint, Update 2, Pop 2, Unwind]),
    ("/", 2, [Push 1, Eval, Push 1, Eval, Divint, Update 2, Pop 2, Unwind]),
    ("negate", 1, [Push 0, Eval, Negint, Update 1, Pop 1, Unwind]),
    ("==", 2, [Push 1, Eval, Push 1, Eval, Eqint, Update 2, Pop 2, Unwind]),
    ("~=", 2, [Push 1, Eval, Push 1, Eval, Neint, Update 2, Pop 2, Unwind]),
    ("<", 2, [Push 1, Eval, Push 1, Eval, Ltint, Update 2, Pop 2, Unwind]),
    ("<=", 2, [Push 1, Eval, Push 1, Eval, Leint, Update 2, Pop 2, Unwind]),
    (">", 2, [Push 1, Eval, Push 1, Eval, Gtint, Update 2, Pop 2, Unwind]),
    (">=", 2, [Push 1, Eval, Push 1, Eval, Geint, Update 2, Pop 2, Unwind]),
    ("+.", 2, [Push 1, Eval, Push 1, Eval, Addfloat, Update 2, Pop 2, Unwind]),
    ("-.", 2, [Push 1, Eval, Push 1, Eval, Subfloat, Update 2, Pop 2, Unwind]),
    ("*.", 2, [Push 1, Eval, Push 1, Eval, Mulfloat, Update 2, Pop 2, Unwind]),
    ("/.", 2, [Push 1, Eval, Push 1, Eval, Divfloat, Update 2, Pop 2, Unwind]),
    ("negateFloat", 1, [Push 0, Eval, Negfloat, Update 1, Pop 1, Unwind]),
    ("==.", 2, [Push 1, Eval, Push 1, Eval, Eqfloat, Update 2, Pop 2, Unwind]),
    ("~=.", 2, [Push 1, Eval, Push 1, Eval, Nefloat, Update 2, Pop 2, Unwind]),
    ("<.", 2, [Push 1, Eval, Push 1, Eval, Ltfloat, Update 2, Pop 2, Unwind]),
    ("<=.", 2, [Push 1, Eval, Push 1, Eval, Lefloat, Update 2, Pop 2, Unwind]),
    (">.", 2, [Push 1, Eval, Push 1, Eval, Gtfloat, Update 2, Pop 2, Unwind]),
    (">=.", 2, [Push 1, Eval, Push 1, Eval, Gefloat, Update 2, Pop 2, Unwind]),
    ("==#", 2, [Push 1, Eval, Push 1, Eval, Eqchar, Update 2, Pop 2, Unwind]),
    ("~=#", 2, [Push 1, Eval, Push 1, Eval, Nechar, Update 2, Pop 2, Unwind]),
    ("<#", 2, [Push 1, Eval, Push 1, Eval, Ltchar, Update 2, Pop 2, Unwind]),
    ("<=#", 2, [Push 1, Eval, Push 1, Eval, Lechar, Update 2, Pop 2, Unwind]),
    (">#", 2, [Push 1, Eval, Push 1, Eval, Gtchar, Update 2, Pop 2, Unwind]),
    (">=#", 2, [Push 1, Eval, Push 1, Eval, Gechar, Update 2, Pop 2, Unwind]),
    ("if", 3, [Push 0, Eval, Cond [Push 1] [Push 2], Update 3, Pop 3, Unwind]),
    ("asFloat", 1, [Push 0, Eval, Asfloat, Update 1, Pop 1, Unwind]),
    ("chr", 1, [Push 0, Eval, Chr, Update 1, Pop 1, Unwind]),
    ("ord", 1, [Push 0, Eval, Ord, Update 1, Pop 1, Unwind]),
    ("floor", 1, [Push 0, Eval, Floor, Update 1, Pop 1, Unwind]),
    ("ceiling", 1, [Push 0, Eval, Floor, Update 1, Pop 1, Unwind]),
    ("round", 1, [Push 0, Eval, Floor, Update 1, Pop 1, Unwind]),
    ("&", 2, [Push 0,Eval,Casejump (aFromList [(1,[Split 0,Pack 1 0,Eval,Slide 0]),(2,[Split 0,Push 1,Eval,Slide 0])]),Update 2,Pop 2,Unwind]),
    ("|", 2, [Push 0,Eval,Casejump (aFromList [(1,[Split 0,Push 1,Eval,Slide 0]),(2,[Split 0,Pack 2 0,Eval,Slide 0])]),Update 2,Pop 2,Unwind])]

allocateSc :: GHeap -> GCompiledSC -> (GHeap, (CName, Addr))
allocateSc heap (name, nargs, instns) = (newHeap, (name, addr)) where
    (newHeap, addr) = hAlloc heap (NGlobal nargs instns)

initialCode :: GCode
initialCode = [Pushglobal "main", Eval, Print]

compileSc :: (CName, [CName], CoreExpr) -> GCompiledSC
compileSc (name, env, body) = (name, length env, compileR body (Map.fromList $ zip env [0..]))

type GCompiler = CoreExpr -> GEnvironment -> GCode
type GEnvironment = ASSOC CName Int

compileR :: GCompiler
compileR e args = compileE e args ++ [Update d, Pop d, Unwind] where
    d = length args

compileC :: GCompiler
compileC (CECon t 0) args = [Pack t 0]
compileC (CECon t a) args = [Pushglobal $ "C{" ++ show t ++ "," ++ show a ++ "}"]
compileC (CELet recursive defs e) args
    | recursive = compileLetrec compileC defs e args
    | otherwise = compileLet compileC defs e args
compileC (CEVar v) env
    | elem v (aDomain env) = [Push n]
    | otherwise = [Pushglobal v]
    where n = aLookup env v (error "This case cannot theoretically happen.")
compileC (CEInt n) env = [Pushint n]
compileC (CEFloat n) env = [Pushfloat n]
compileC (CEChar n) env = [Pushchar n]
compileC (CEApp e1 e2) args
    | saturatedCons spine = compileCS (reverse spine) args
    | otherwise = compileC e2 args ++ compileC e1 (argOffset 1 args) ++ [Mkap]
    where spine = makeSpine (CEApp e1 e2)
          saturatedCons (CECon t a : es) = a == length es
          saturatedCons _ = False
makeSpine :: CoreExpr -> [CoreExpr]
makeSpine (CEApp e1 e2) = makeSpine e1 ++ [e2]
makeSpine e = [e]
compileCS :: [CoreExpr] -> GEnvironment -> GCode
compileCS (CECon t a : []) args = [Pack t a]
compileCS (e : es) args = compileC e args ++ compileCS es (argOffset 1 args)


compileAlts :: (Int -> GCompiler) -> [CoreAlt] -> GEnvironment -> ASSOC Int GCode
compileAlts comp alts env
    = aFromList [(tag, comp (length names) body (aUnion (aFromList $ zip names [0..]) (argOffset (length names) env))) | (tag, names, body) <- alts]

compileE' :: Int -> GCompiler
compileE' offset expr env = [Split offset] ++ compileE expr env ++ [Slide offset]

compileLet :: GCompiler -> [(CName, CoreExpr)] -> GCompiler
compileLet comp defs expr env = compileLet' defs env ++ comp expr env' ++ [Slide (length defs)] where
    env' = compileArgs defs env
compileLet' :: [(CName, CoreExpr)] -> GEnvironment -> GCode
compileLet' [] env = []
compileLet' ((name, expr) : defs) env = compileC expr env ++ compileLet' defs (argOffset 1 env)
compileArgs :: [(CName, CoreExpr)] -> GEnvironment -> GEnvironment
compileArgs defs env = aUnion (aFromList $ zip (map fst defs) [n-1, n-2 .. 0]) (argOffset n env) where
    n = length defs

compileLetrec :: GCompiler -> [(CName, CoreExpr)] -> GCompiler
compileLetrec comp defs e args
    = (Alloc (length defs)) : (localDefined defs (n - 1) ++ comp e newArgs) where
        newArgs = compileArgs defs args
        n = length defs
        localDefined [] i = []
        localDefined (d : ds) i = compileC (snd d) newArgs ++ [Update i] ++ localDefined ds (i - 1)

argOffset :: Int -> GEnvironment -> GEnvironment
argOffset n = aMapvalue (+n)

compileE :: GCompiler
compileE (CEInt n) arg = [Pushint n]
compileE (CEFloat n) arg = [Pushfloat n]
compileE (CEChar n) arg = [Pushchar n]
compileE (CECase e alts) arg = compileE e arg ++ [Casejump $ compileAlts compileE' alts arg]
compileE (CEApp (CEApp (CEVar op) e1) e2) args
    | op `elem` binaryOps
        = compileE e2 args ++ compileE e1 args' ++ [inst] where
            binaryOps = map fst builtInDyadic
            inst = aLookup (aFromList builtInDyadic) op (error "This case cannot theoretically happen.")
            args' = argOffset 1 args
compileE (CEApp (CEVar "negate") e) args = compileE e args ++ [Negint]
compileE (CEApp (CEVar "negateFloat") e) args = compileE e args ++ [Negfloat]
compileE (CEApp (CEVar "asFloat") e) args = compileE e args ++ [Asfloat]
compileE (CEApp (CEVar "chr") e) args = compileE e args ++ [Chr]
compileE (CEApp (CEVar "ord") e) args = compileE e args ++ [Ord]
compileE (CEApp (CEApp (CEApp (CEVar "if") e1) e2) e3) args
    = compileE e1 args ++ [Cond (compileE e2 args) (compileE e3 args)]
compileE e arg = compileC e arg ++ [Eval]
builtInDyadic :: [(CName, GInstruction)]
builtInDyadic = [
    ("+", Addint), ("-", Subint), ("*", Mulint), ("/", Divint),
    ("==", Eqint), ("~=", Neint), (">=", Geint), (">", Gtint), ("<=", Leint), ("<", Ltint),
    ("+.", Addfloat), ("-.", Subfloat), ("*.", Mulfloat), ("/.", Divfloat),
    ("==.", Eqfloat), ("~=.", Nefloat), (">=.", Gefloat), (">.", Gtfloat), ("<=.", Lefloat), ("<.", Ltfloat),
    ("==_", Eqchar), ("~=_", Nechar), (">=_", Gechar), (">_", Gtchar), ("<=_", Lechar), ("<_", Ltchar)]

boxInteger :: Int -> GState -> GState
boxInteger n state = putStack newStack $ putHeap newHeap $ state where
    (newHeap, a) = hAlloc (getHeap state) (NInt n)
    newStack = a : getStack state

unboxInteger :: Addr -> GState -> Int
unboxInteger a state = ub (hLookup (getHeap state) a) where
    ub (NInt i) = i
    ub _ = error "Unboxing a non-integer"

boxFloat :: Double -> GState -> GState
boxFloat n state = putStack newStack $ putHeap newHeap $ state where
    (newHeap, a) = hAlloc (getHeap state) (NFloat n)
    newStack = a : getStack state

unboxFloat :: Addr -> GState -> Double
unboxFloat a state = ub (hLookup (getHeap state) a) where
    ub (NFloat i) = i
    ub _ = error "Unboxing a non-float"

boxChar :: Char -> GState -> GState
boxChar n state = putStack newStack $ putHeap newHeap $ state where
    (newHeap, a) = hAlloc (getHeap state) (NChar n)
    newStack = a : getStack state

unboxChar :: Addr -> GState -> Char
unboxChar a state = ub (hLookup (getHeap state) a) where
    ub (NChar i) = i
    ub _ = error "Unboxing a non-char"

boxBoolean :: Bool -> GState -> GState
boxBoolean b state = putStack newStack $ putHeap newHeap $ state where
    (newHeap, a) = hAlloc (getHeap state) (NConstr b' [])
    newStack = a : getStack state
    b' = if b then 2 else 1

primitive1 :: (b -> GState -> GState) -- boxing function.
           -> (Addr -> GState -> a) -- unboxing function
           -> (a -> b) -- the operator.
           -> (GState -> GState)
primitive1 box unbox op state = box (op (unbox a state)) (putStack as state) where
    (a : as) = getStack state

primitive2 :: (b -> GState -> GState)
           -> (Addr -> GState -> a)
           -> (a -> a -> b)
           -> (GState -> GState)
primitive2 box unbox op state = box (op (unbox a0 state) (unbox a1 state)) (putStack as state) where
    (a0 : a1 : as) = getStack state

-- 我们不做重载是因为操作符的重载是用typeclass来做的而我们又不打算做typeclass……
-- 虽然可以上dictionary passing，不过看着会很脏。
intarithmetic1 :: (Int -> Int) -> (GState -> GState)
intarithmetic1 = primitive1 boxInteger unboxInteger
intarithmetic2 :: (Int -> Int -> Int) -> (GState -> GState)
intarithmetic2 = primitive2 boxInteger unboxInteger
floatarithmetic1 :: (Double -> Double) -> (GState -> GState)
floatarithmetic1 = primitive1 boxFloat unboxFloat
floatarithmetic2 :: (Double -> Double -> Double) -> (GState -> GState)
floatarithmetic2 = primitive2 boxFloat unboxFloat

intcomparison :: (Int -> Int -> Bool) -> GState -> GState
intcomparison = primitive2 boxBoolean unboxInteger
floatcomparison :: (Double -> Double -> Bool) -> GState -> GState
floatcomparison = primitive2 boxBoolean unboxFloat
charcomparison :: (Char -> Char -> Bool) -> GState -> GState
charcomparison = primitive2 boxBoolean unboxChar

-- pretty printing.
-- TODO: haskell有prettyprinting的库。用那个把现在的这个换掉。
showResults :: [GState] -> String
showResults = concat . (map ((++"\n") . showState))
showState :: GState -> String
showState s = concat [
    "State:\n",
    "Output: ", show (getOutput s), "\n",
    "Code: ", showCode (getCode s), "\n",
    "Stack: ", showStack (getStack s), "\n",
    "Dump: ", showDump (getDump s), "\n",
    "Heap: ", showHeap (getHeap s), "\n",
    "Globals: ", showGlobals (getGlobals s), "\n",
    "Stats: ", showStats (getStats s), "\n"]
showCode :: GCode -> String
showCode = show
showStack :: GStack -> String
showStack = show
showDump :: GDump -> String
showDump d = concat ("Dump: [" : (map showDumpItem (reverse d) ++ ["]"]))
showDumpItem :: GDumpItem -> String
showDumpItem (code, stack) = concat [
    "<", shortShowCode 3 code, ",",
    shortShowStack stack, ">"]
shortShowCode :: Int -> GCode -> String
shortShowCode n c = concat [
    "{", concat showed, "}"] where
        showed
            | length c > n = map (\x-> show x ++ ";") (take n c) ++ ["..."]
            | otherwise = map (\x-> show x ++ ";") c
shortShowStack :: GStack -> String
shortShowStack x
    | length x > 3 = show x ++ " and more"
    | otherwise = show x
showHeap :: GHeap -> String
showHeap (size, free, mem) = concat [
    "Size: ", show size, " Free: ", show $ head free, " Mem: ", show mem]
showGlobals :: GGlobals -> String
showGlobals = show
showStats :: GStats -> String
showStats = show

