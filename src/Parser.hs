module Parser (
    TokenParser
,   program
,   parseTokens
,   parseString
,   unwrapProgram
) where
import Language
import Tokenizer as T
import Text.Parsec.Prim
import Text.Parsec.Pos
import Text.Parsec.Combinator
import Data.List (isPrefixOf)

type BaseTokenParser a = Parsec [Token] () a
type TokenParser a = BaseTokenParser (Maybe a)

checkJust (Just _) = True
checkJust _ = False

fromJust (Just x) = x

fromRight (Right x) = x


parserWrapper :: BaseTokenParser a -> TokenParser a
parserWrapper p = try (do {
    res <- p;
    return $ Just res
}) <|> return Nothing

satisfy_ :: (TokenType -> Bool) -> BaseTokenParser Token
satisfy_ f = tokenPrim show nextTokenPos match
           where match x@(t, _, _) = if f t then Just x else Nothing

satisfy :: (TokenType -> Bool) -> TokenParser Token
satisfy = parserWrapper . satisfy_

int = satisfy_ (\x-> case x of { IntLiteral _ -> True; _ -> False }) <?> "integer literal"
float = satisfy_ (\x-> case x of { FloatLiteral _ -> True; _ -> False }) <?> "float number literal"
char = satisfy_ (\x-> case x of { CharLiteral _ -> True; _ -> False }) <?> "char literal"
lparen = satisfy_ (== LParen) <?> "left parenthesis \"(\""
rparen = satisfy_ (== RParen) <?> "right parenthesis \")\""
lbrace = satisfy_ (== LBrace) <?> "left brace \"{\""
rbrace = satisfy_ (== RBrace) <?> "left brace \"}\""
semicolon = satisfy_ (== Semicolon) <?> "semicolon \";\""
slash = satisfy_ (== Slash) <?> "slash \"\\\""
rarrow = satisfy_ (== RightArrow) <?> "right arrow \"->\""
white = choice [whitespace, newline, comment] <?> "whitespace, newline or comment"
whitespace = satisfy_ (\x-> case x of { Whitespace _ -> True; _ -> False }) <?> "whitespace"
newline = satisfy_ (\x-> case x of { Newline _ -> True; _ -> False }) <?> "newline"
comment = satisfy_ (\x-> case x of { Comment _ -> True; _ -> False }) <?> "comment"
wildcard = satisfy_ (== Wildcard) <?> "wildcard"
varid = satisfy_ (\x-> case x of { Varid _ -> True; _ -> False }) <?> "variable identifier"
constid = satisfy_ (\x-> case x of { Constid _ -> True; _ -> False }) <?> "constructor identifier"
midfixop = satisfy_ (\x-> case x of { MidfixOp _ -> True; _ -> False }) <?> "midfix operator"
symbolop = satisfy_ (\x-> case x of { SymbolOp _ -> True; _ -> False }) <?> "symbol operator"
symbolopOf s = satisfy_ (\x-> case x of { SymbolOp x -> x == s; _ -> False }) <?> ("symbol operator " ++ s)
expr1oplst = ["&", "|"]
expr1op = satisfy_ (\x-> case x of { SymbolOp x -> x `elem` expr1oplst; _ -> False}) <?> ("symbol operator in " ++ show expr1oplst)
expr2oplst = ["<", "<=", "==", "~=", ">=", ">"]
checkPrefixOf x y = any (\pfix-> pfix `isPrefixOf` y) x
expr2op = satisfy_ (\x-> case x of { SymbolOp x -> checkPrefixOf expr2oplst x; _ -> False}) <?> ("symbol operator with a prefix in " ++ show expr2oplst)
zthop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t == '!'; _ -> False }) <?> "symbol operator started with !"
athop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t == '#'; _ -> False }) <?> "symbol operator started with #"
bthop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t `elem` "*/%"; _ -> False }) <?> "symbol operator started with *, / or %"
-- --的可能已经通过tokenizer排除掉了。
cthop = satisfy_ (\x-> case x of { SymbolOp tk@(t:_) -> t == '+' || t == '-'; _ -> False }) <?> "symbol operator started with + or -"
fthop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t == '?'; _ -> False }) <?> "symbol operator started with ?"
gthop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t == '^'; _ -> False }) <?> "symbol operator started with ^"
hthop = satisfy_ (\x-> case x of { SymbolOp (t:_) -> t == '$'; _ -> False }) <?> "symbol operator started with $"
reserved x = satisfy_ (== x) <?> ("reserved word " ++ show x)

pat = choice [
    do {
        h <- constid;
        r <- many (try $ many white >> varid);
        return $ PCon h r
    },
    do {
        lparen; many white;
        h <- constid;
        r <- many (try $ many white >> varid);
        many white; rparen;
        return $ PCon h r
    }] <?> "pattern"
pats = do {
    h <- pat;
    r <- many (try $ many white >> pat);
    return $ h:r
} <?> "list of patterns"

atomexpr = choice [
    do {
        lparen; many white;
        x <- expr;
        many white; rparen;
        return $ AExpr x
    },
    varid >>= return . AVar,
    constid >>= return . ACon,
    int >>= return . AInt,
    float >>= return . AFloat,
    char >>= return . AChar] <?> "atomic expression"

mkRRecursive body sep singlf multif = do {
    h <- body;
    r <- many (try $ sep >> body);
    return $ f h r
} where f h [] = singlf h
        f h (x:xs) = multif h (f x xs)

mkLRecursive body sep singlf multif = do {
    h <- body;
    r <- many (try $ sep >> body);
    return $ f h (reverse r)
} where f h [] = singlf h
        f h (x:xs) = multif (f h xs) x

mkTrailerP sep body = sep >>= \s-> body >>= \b-> return (s, b)
wrapInWhite x = between (many white) (many white) x

lrecursivef h [] = h
lrecursivef h ((op, body):xs) = EOp (lrecursivef h xs) op body
rrecursivef h [] = h
rrecursivef h ((op, body):xs) = EOp h op (rrecursivef body xs)
mklropexpr body sep = do {
    h <- body;
    r <- many (try $ mkTrailerP (wrapInWhite sep) body);
    return $ lrecursivef h (reverse r)
}
mkrropexpr body sep = do {
    h <- body;
    r <- many (try $ mkTrailerP (wrapInWhite sep) body);
    return $ rrecursivef h r
}

funcappexpr = mkLRecursive atomexpr (many white) EAtom EApp <?> "function application expr."
-- 1. 中缀op左结合。
-- 2. 中缀op优先级高于所有符号op，低于函数应用。
midfixexpr = mklropexpr funcappexpr midfixop <?> "midfix expr."
zthexpr = mklropexpr midfixexpr zthop <?> "zth expr."
athexpr = mkrropexpr zthexpr athop <?> "ath expr."
bthexpr = mklropexpr athexpr bthop <?> "bth expr."
cthexpr = mklropexpr bthexpr cthop <?> "cth expr."
fthexpr = mklropexpr cthexpr fthop <?> "fth expr."
gthexpr = mkrropexpr fthexpr gthop <?> "gth expr."
hthexpr = mkrropexpr gthexpr hthop <?> "hth expr."
expr3 = hthexpr <?> "algorithmic expression"
expr2 = do {
    l <- expr3;
    r <- optionMaybe (try $ wrapInWhite expr2op >>= \op-> expr3 >>= \r-> return (op, r));
    return $ f l r
} <?> "relational expression"
    where f l Nothing = l
          f l (Just (op, r)) = EOp l op r
expr1 = mklropexpr expr2 expr1op <?> "boolean expression"

condexpr = do {
    reserved If;
    wrapInWhite lbrace;
    branches <- branches;
    many white; rbrace;
    return $ EIf branches
} <?> "conditional expression"
branches = do {
    h <- branch;
    r <- many (try $ wrapInWhite semicolon >> branch);
    return $ (h:r)
} <?> "list of conditional branches"
branch = do {
    h <- expr;
    wrapInWhite rarrow;
    r <- expr;
    return $ (h, r)
} <?> "conditional branch"

letexpr = do {
    reserved Let; wrapInWhite lbrace;
    r <- decls;
    wrapInWhite rbrace; wrapInWhite (reserved In);
    b <- expr;
    return $ ELet r b 
} <?> "let expression"
letrecexpr = do {
    reserved Letrec; wrapInWhite lbrace;
    r <- decls;
    wrapInWhite rbrace; wrapInWhite (reserved In);
    b <- expr;
    return $ ELetrec r b 
} <?> "letrec expression"
decls = do {
    h <- decl;
    r <- many (try $ (wrapInWhite semicolon) >> decl);
    return $ (h:r)
} <?> "list of value declarations"
decl = valdef
valdef = do {
    l <- lhs;    
    wrapInWhite (symbolopOf "=");
    r <- expr;
    return $ (l, r)
} <?> "value declaration"
lhs = do {
    h <- varid;
    r <- many (try (many1 white >> varid));
    return $ (h, r)
} <?> "left hand side"

caseexpr = do {
    reserved Case; many white;
    h <- expr;
    wrapInWhite (reserved Of);
    wrapInWhite lbrace;
    r <- alts;
    many white; rbrace;
    return $ ECase h r
} <?> "case expression"
alts = do {
    h <- alt;
    r <- many (try $ wrapInWhite semicolon >> alt);
    return $ (h : r)
} <?> "list of case alternates"
alt = do {
    h <- pat;
    wrapInWhite rarrow;
    r <- expr;
    return $ (h, r)
} <?> "case alternate"

lambdaexpr =
    do {
        slash >> many white;
        p <- varidlst;
        wrapInWhite rarrow;
        body <- expr;
        return $ ELambda p body
    } <?> "lambda expression"
    where varidlst = do {
        h <- varid;
        r <- many (try (many1 white >> varid));
        return $ (h:r)
    } <?> "list of variable identifiers"

expr = choice [
    letrecexpr, letexpr, condexpr, caseexpr, expr1, lambdaexpr,
    do {
        wrapInWhite lparen;
        e <- expr;
        many white; rparen;
        return $ e
    }] <?> "expression"

datadef = do {
    reserved Data;
    many1 white;
    newtypehead <- constid;
    many white;
    arity <- int;
    return $ Datadef newtypehead (read $ readToken arity)
} <?> "constructor declaration" where readToken (IntLiteral x, _, _) = x

stmt = do {
    s <- choice [datadef, valdef >>= return . Valdef];
    many white >> semicolon >> many white;
    return s
} <?> "statement"

program_ = many stmt;

program [] = []
program tokenstr = case parse myparse "error while parsing" tokenstr of {
    t@(Left x) -> error $ show x;
    Right (sc, rest) -> sc : program rest
} where {
    myparse = do {
        r <- stmt;
        x <- getInput;
        return (r, x)
    }
}

parseTokens = program . removeTrailingWhite . removeComment
parseString = program . removeTrailingWhite . removeComment . tokenize

removeTrailingWhite = dropWhile isWhite where
    isWhite (Whitespace _, _, _) = True
    isWhite (Newline _, _, _) = True
    isWhite _ = False
    

unwrapExpr (ELambda args body) = ELambda (map getTokenStr args) (unwrapExpr body)
unwrapExpr (ELet decls body) = ELet (unwrapDecls decls) (unwrapExpr body)
unwrapExpr (ELetrec decls body) = ELetrec (unwrapDecls decls) (unwrapExpr body)
unwrapExpr (EIf branches) = EIf (unwrapBranches branches)
unwrapExpr (EOp l m r) = EOp (unwrapExpr l) (getTokenStr m) (unwrapExpr r)
unwrapExpr (EApp l r) = EApp (unwrapExpr l) (unwrapAtomexp r)
unwrapExpr (ECase x alters) = ECase (unwrapExpr x) (unwrapAlters alters)
unwrapExpr (EAtom x) = EAtom (unwrapAtomexp x)
unwrapAtomexp (AInt x) = AInt (getTokenStr x)
unwrapAtomexp (AFloat x) = AFloat (getTokenStr x)
unwrapAtomexp (AChar x) = AChar (getTokenStr x)
unwrapAtomexp (ACon x) = ACon (getTokenStr x)
unwrapAtomexp (AVar x) = AVar (getTokenStr x)
unwrapAtomexp (AExpr x) = AExpr (unwrapExpr x)
unwrapDecls = map unwrapDecl
unwrapDecl (lhs, expr) = (unwrapLhs lhs, unwrapExpr expr)
unwrapLhs (x, args) = (getTokenStr x, map getTokenStr args)
unwrapBranches = map unwrapBranch
unwrapBranch (x, y) = (unwrapExpr x, unwrapExpr y)
unwrapAlters = map unwrapAlter
unwrapAlter (x, y) = (unwrapPat x, unwrapExpr y)
unwrapPat (PCon x y) = PCon (getTokenStr x) (map getTokenStr y)
unwrapTopDecl (Datadef x y) = Datadef (getTokenStr x) y
unwrapTopDecl (Valdef x) = Valdef (unwrapDecl x)
unwrapProgram = map unwrapTopDecl