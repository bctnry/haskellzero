module LambdaLifting (
    lambdaLift
) where
import Core
import Utils
import Data.List (mapAccumL)
import qualified Data.Set as Set

type AnnExpr a b = (b, AnnExpr' a b)
data AnnExpr' a b
    = ACVar CName
    | ACInt Int | ACFloat Double | ACChar Char
    | ACCon Int Int
    | ACApp (AnnExpr a b) (AnnExpr a b)
    | ACLet Bool [AnnDefn a b] (AnnExpr a b)
    | ACCase (AnnExpr a b) [AnnAlt a b]
    | ACLam [a] (AnnExpr a b)
    deriving (Eq, Show)
type AnnDefn a b = (a, AnnExpr a b)
type AnnAlt a b = (Int, [a], (AnnExpr a b))
type AnnProgram a b = [(CName, [a], AnnExpr a b)]

lambdaLift :: CoreProgram -> CoreProgram
lambdaLift = collectSCs . rename . abstract . freeVars

freeVars :: CoreProgram -> AnnProgram CName (Set.Set CName)
freeVars = map (\(name, args, body)-> (name, args, freeVarsE (Set.fromList args) body))

-- retrieving free vars.
freeVarsE :: (Set.Set CName) -- 所有出现的变量的集合
           -> CoreExpr -- 待求的表达式
           -> AnnExpr CName (Set.Set CName) -- 结果
freeVarsE lv (CEInt n) = (Set.empty, ACInt n)
freeVarsE lv (CEFloat n) = (Set.empty, ACFloat n)
freeVarsE lv (CEChar n) = (Set.empty, ACChar n)
freeVarsE lv (CEVar v)
    | Set.member v lv = (Set.singleton v, ACVar v)
    | otherwise = (Set.empty, ACVar v)
freeVarsE lv (CEApp e1 e2)
    = (Set.union (freeVarsOf e1') (freeVarsOf e2'), ACApp e1' e2') where
        e1' = freeVarsE lv e1
        e2' = freeVarsE lv e2
freeVarsE lv (CELambda args body)
    = ((freeVarsOf body') Set.\\ (Set.fromList args), ACLam args body') where
        body' = freeVarsE new_lv body
        new_lv = Set.union lv (Set.fromList args)
freeVarsE lv (CELet isRec defns body)
    = (Set.union defnsFree bodyFree, ACLet isRec defns' body') where
        binders = bindersOf defns
        binderSet = Set.fromList binders
        bodyLv = Set.union lv binderSet
        rhsLv
            | isRec = bodyLv
            | otherwise = lv
        rhss' = map (freeVarsE rhsLv) (rhssOf defns)
        defns' = zip binders rhss'
        freeInValues = Set.unions (map freeVarsOf rhss')
        defnsFree
            | isRec = freeInValues Set.\\ binderSet
            | otherwise = freeInValues
        body' = freeVarsE bodyLv body
        bodyFree = (freeVarsOf body') Set.\\ binderSet
freeVarsE lv (CECase e alts) = freeVars_case lv e alts
-- TODO: check if this is right.
freeVarsE lv (CECon t a) = (Set.empty, ACCon t a)
freeVars_case lv e alts = (Set.union (freeVarsOf e') free, ACCase e' alts') where
    e' = freeVarsE lv e
    alts' = [(tag, args, freeVarsE (Set.union lv (Set.fromList args)) e) | (tag, args, e) <- alts]
    free = Set.unions (map freeVarsOf_alt alts')

freeVarsOf :: AnnExpr CName (Set.Set CName)-> Set.Set CName
freeVarsOf (freeVars, expr) = freeVars
freeVarsOf_alt :: AnnAlt CName (Set.Set CName) -> Set.Set CName
freeVarsOf_alt (tag, args, rhs) = (freeVarsOf rhs) Set.\\ (Set.fromList args)


abstract :: AnnProgram CName (Set.Set CName) -> CoreProgram
abstract = map (\(scName, args, rhs)-> (scName, args, abstractE rhs))
abstractE :: AnnExpr CName (Set.Set CName) -> CoreExpr
abstractE (free, ACVar v) = CEVar v
abstractE (free, ACInt n) = CEInt n
abstractE (free, ACFloat n) = CEFloat n
abstractE (free, ACChar c) = CEChar c
abstractE (free, ACApp e1 e2) = CEApp (abstractE e1) (abstractE e2)
abstractE (free, ACLet isRec defns body)
    = CELet isRec [(name, abstractE body) | (name, body) <- defns] (abstractE body)
abstractE (free, ACLam args body) = foldl CEApp sc (map CEVar fvList) where
    fvList = Set.toList free
    sc = CELet nonRecursive [("sc", sc_rhs)] (CEVar "sc")
    sc_rhs = CELambda (fvList ++ args) (abstractE body)
abstractE (free, ACCon t a) = CECon t a
abstractE (free, ACCase e alts) = abstractCase free e alts
abstractCase free e alts = CECase (abstractE e) [(tag, args, abstractE e) | (tag, args, e) <- alts]



rename :: CoreProgram -> CoreProgram
rename prog
    = snd (mapAccumL renameSC initialNameSupply prog) where
        renameSC ns (scName, args, rhs) = (ns2, (scName, args', rhs')) where
            (ns1, args', env) = newNames ns args
            (ns2, rhs') = renameE env ns1 rhs
newNames :: NameSupply -> [CName] -> (NameSupply, [CName], ASSOC CName CName)
newNames ns oldNames = (ns', newNames, env) where
    (ns', newNames) = getNames ns oldNames
    env = aFromList (zip oldNames newNames)


renameE :: ASSOC CName CName -> NameSupply -> CoreExpr -> (NameSupply, CoreExpr)
renameE env ns (CEVar v) = (ns, CEVar (aLookup env v v))
renameE env ns (CEInt n) = (ns, CEInt n)
renameE env ns (CEFloat n) = (ns, CEFloat n)
renameE env ns (CEChar n) = (ns, CEChar n)
renameE env ns (CEApp e1 e2) = (ns2, CEApp e1' e2') where
    (ns1, e1') = renameE env ns e1
    (ns2, e2') = renameE env ns1 e2
renameE env ns (CELambda args body) = (ns1, CELambda args' body') where
    (ns1, args', env') = newNames ns args
    (ns2, body') = renameE (aUnion env' env) ns1 body
renameE env ns (CELet isRec defns body) = (ns3, CELet isRec (zip binders' rhss') body') where
    (ns1, body') = renameE body_env ns body
    binders = bindersOf defns
    (ns2, binders', env') = newNames ns1 binders
    body_env = aUnion env' env
    (ns3, rhss') = mapAccumL (renameE rhsEnv) ns2 (rhssOf defns)
    rhsEnv
        | isRec = body_env
        | otherwise = env
renameE env ns (CECon t a) = (ns, CECon t a)
renameE env ns (CECase e alts) = renameCase env ns e alts
renameCase env ns e alts = (ns2, CECase e' alts') where
    (ns1, e') = renameE env ns e
    (ns2, alts') = mapAccumL renameAlt ns alts
    renameAlt ns (tag, args, rhs) = (ns2, (tag, args', rhs')) where
        (ns1, args', env') = newNames ns args
        (ns2, rhs') = renameE (aUnion env' env) ns1 rhs




collectSCs :: CoreProgram -> CoreProgram
collectSCs prog = concat (map collectOneSc prog) where
    collectOneSc (scName, args, rhs) = (scName, args, rhs') : scs where
        (scs, rhs') = collectSCsE rhs

collectSCsE :: CoreExpr -> ([CoreScDefn], CoreExpr)
collectSCsE (CEVar n) = ([], CEVar n)
collectSCsE (CEInt n) = ([], CEInt n)
collectSCsE (CEFloat n) = ([], CEFloat n)
collectSCsE (CEChar n) = ([], CEChar n)
collectSCsE (CEApp e1 e2) = (scs1 ++ scs2, CEApp e1' e2') where
    (scs1, e1') = collectSCsE e1
    (scs2, e2') = collectSCsE e2
collectSCsE (CELambda args body) = (scs, CELambda args body') where
    (scs, body') = collectSCsE body
collectSCsE (CECon t a) = ([], CECon t a)
collectSCsE (CECase e alts) = (scsE ++ scsAlts, CECase e' alts') where
    (scsE, e') = collectSCsE e
    (scsAlts, alts') = mapAccumL collectSCsAlt [] alts
    collectSCsAlt scs (tag, args, rhs) = (scs ++ scsRhs, (tag, args, rhs')) where
        (scsRhs, rhs') = collectSCsE rhs
collectSCsE (CELet isRec defns body)
    = (rhssScs ++ bodyScs ++ localScs, mkCELet isRec nonScs' body') where
        (rhssScs, defns') = mapAccumL collectSCsD [] defns
        scs' = [(name, rhs) | (name, rhs) <- defns', isCELam rhs] 
        nonScs' = [(name, rhs) | (name, rhs) <- defns', not (isCELam rhs)]
        localScs = [(name, args, body) | (name, CELambda args body) <- scs']
        (bodyScs, body') = collectSCsE body
        collectSCsD scs (name, rhs) = (scs ++ rhsScs, (name, rhs')) where
            (rhsScs, rhs') = collectSCsE rhs
isCELam :: CExpr a -> Bool
isCELam (CELambda args body) = True
isCELam _ = False
mkCELet = CELet


