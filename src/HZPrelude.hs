module HZPrelude (
    preludeConstructors
,   preludeFunctions
) where
import Utils

preludeConstructors :: ASSOC String (Int, Int)
preludeConstructors = aFromList [
    ("Unit", (0, 0)),
    ("False", (1, 0)), ("True", (2, 0)),
    ("MkTuple", (3, 2)),
    ("Nil", (4, 0)), ("Cons", (5, 2)),
    ("Nothing", (6, 0)), ("Just", (7, 1))]
    
preludeFunctions = concat [
    "and x y = case x of {\n",
    "    True -> y;\n",
    "    False -> False\n",
    "};\n",
    "or x y = case x of {\n",
    "    True -> True;\n",
    "    False -> y\n",
    "};\n",
    "not x = case x of {\n",
    "    True -> False;\n",
    "    False -> True\n",
    "};\n",
    "otherwise = True;\n",
    "fst x = case x of {\n",
    "    MkTuple a b -> a\n",
    "};\n",
    "snd x = case x of {\n",
    "    MkTuple a b -> b\n",
    "};\n",
    "map f l = case l of {\n",
    "    Nil -> Nil;\n",
    "    Cons x y -> Cons (f x) (map f y)\n",
    "};\n",
    "filter f l = case f of {\n",
    "    Nil -> Nil;\n",
    "    Cons x y -> if {\n",
    "        f x -> Cons x (filter f y);\n",
    "        otherwise -> filter f y\n",
    "    }\n",
    "};\n",
    "foldl f b l = case l of {\n",
    "    Nil -> b;\n",
    "    Cons a as -> foldr f (f a b) as\n",
    "};\n",
    "foldr f b l = case l of {\n",
    "    Nil -> b;\n",
    "    Cons a as -> foldr f (f a b) as\n",
    "};\n",
    "id x = x;\n",
    "const x y = x;\n",
    "subst x y z = x z (y z);\n",
    "const1 x y = y;\n",
    "flip f a b = f b a;\n"]