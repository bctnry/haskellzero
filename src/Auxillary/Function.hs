module Auxillary.Function (
    (|>)
,   inverse
,   interleave
,   fromRight
) where

(|>) :: (a -> b) -> (b -> c) -> (a -> c)
(|>) f g = g . f

inverse :: (a -> Bool) -> (a -> Bool)
inverse f = \a -> not (f a)

interleave :: String -> [String] -> String
interleave _ [] = ""
interleave sep lst = foldr1 concatf lst
                   where concatf x y = concat [x, sep, y]

fromRight (Right x) = x

