module Language (
    Program
,   TopDecl(..)
,   Decls
,   Pats
,   Pat(..)
,   Lhs
,   ValDef
,   Expr(..)
,   AtomExpr(..)
,   Alter
,   Branches
,   Branch
) where
import Tokenizer
import qualified Text.PrettyPrint as PP

{-
data Nil 0;
data Branch 3;
treemap f x = case x of {
    Nil -> Nil;
    Branch v l r -> Branch (f v) (treemap f l) (treemap f r)
};
factorial x = if {
    x <= 1 -> 1;
    otherwise -> x * factorial (x - 1)
}
-- actually otherwise = True
-}

{-
Program -> TopDecl+{;}
TopDecl -> Datadef | Valdef
Datadef -> "data" Constid IntLiteral
Decls -> Valdef+{;}
Pats -> Pat+{ }
Pat -> Constid Varid+{ } | "(" Constid Varid+{ } ")"
Lhs -> Varid+{ }
Valdef -> Lhs "=" Expr
Expr -> "\" Pats "->" Expr
     || "let" ["rec"] "{" Decls "}" "in" Expr
     || "if" "{" Branch+{;} "}"
     || Expr1
Branch -> Expr "->" Expr
Expr1 -> Expr2 | Expr1 ("&" | "|") Expr2
Expr2 -> Expr3 | Expr3 ("<" | "<=" | "==" | "~=" | ">=" | ">") Expr3
Expr3 -> HthExpr
HthExpr -> GthExpr | GthExpr ^"$" HthExpr
GthExpr -> FthExpr | FthExpr ^"^" GthExpr
FthExpr -> CthExpr | FthExpr ^"?" CthExpr
CthExpr -> BthExpr | CthExpr ^("+" | "-") BthExpr
BthExpr -> AthExpr | BthExpr ^("*" | "/" | "%") AthExpr
AthExpr -> ZthExpr | ZthExpr ^"#" AthExpr
ZthExpr -> MidfixExpr | ZthExpr ^"!" MidfixExpr
MidfixExpr -> FuncAppExpr | MidfixExpr MidfixOp FuncAppExpr
FuncAppExpr -> AtomExpr | FuncAppExpr " " AtomExpr
AtomExpr -> "(" Expr ")"
         || IntLiteral | FloatLiteral | CharLiteral
         || Varid || Constid
-}

type Program a = [TopDecl a]
data TopDecl a = Datadef a Int
               | Valdef (ValDef a)
               deriving (Show)
type Decls a = [ValDef a]
type Pats a = [Pat a]
data Pat a = PCon a [a] deriving (Show)
type Lhs a = (a, [a])
type ValDef a = (Lhs a, Expr a)
data Expr a = ELambda [a] (Expr a)
            | ELet (Decls a) (Expr a)
            | ELetrec (Decls a) (Expr a)
            | EIf (Branches a)
            | EOp (Expr a) a (Expr a)
            | EApp (Expr a) (AtomExpr a)
            | ECase (Expr a) [Alter a]
            | EAtom (AtomExpr a)
            deriving (Show)
type Alter a = (Pat a, Expr a)
type Branches a = [Branch a]
type Branch a = (Expr a, Expr a)
data AtomExpr a = AInt a | AFloat a | AChar a
                | AExpr (Expr a)
                | ACon a
                | AVar a
                deriving (Show)
