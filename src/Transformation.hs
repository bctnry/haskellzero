module Transformation (
    transformProgram
) where
import Language
import HZPrelude
import Core
import Utils
import Parser
import Tokenizer

type ConstrMap = ASSOC CName (Int, Int)

collect :: Program String -> (ConstrMap, Program String)
collect x = (aUnion collectResult preludeConstructors, defns) where
    (collectResult, defns) = collect_ x (aSize preludeConstructors) aEmpty []
    collect_ [] n a rest = (a, reverse rest)
    collect_ ((Datadef name arity) : xs) n a rest
        | aContainKey name a = error $ "error when transforming: duplicate definition for constructor " ++ name
        | otherwise = collect_ xs (n + 1) (aInsert name (n, arity) a) rest
    collect_ (x : xs) n a rest = collect_ xs n a (x : rest)

transformExpr :: ConstrMap -> Expr String -> CExpr String
transformExpr cm (ELambda args body) = CELambda args (transformExpr cm body)
transformExpr cm (ELet decls body) = CELet nonRecursive (transformDecls cm decls) (transformExpr cm body)
transformExpr cm (ELetrec decls body) = CELet recursive (transformDecls cm decls) (transformExpr cm body)
transformExpr cm (EIf branches) = transformIf cm branches
transformExpr cm (EOp l op r) = CEApp (CEApp (CEVar op) (transformExpr cm l)) (transformExpr cm r)
transformExpr cm (EApp l r) = CEApp (transformExpr cm l) (transformAtomexp cm r)
transformExpr cm (EAtom x) = transformAtomexp cm x
transformExpr cm (ECase e as) = CECase (transformExpr cm e) (transformAlters cm as)

transformAtomexp :: ConstrMap -> AtomExpr String -> CExpr String
transformAtomexp cm (AInt x) = CEInt (read x)
transformAtomexp cm (AFloat x) = CEFloat (read x)
transformAtomexp cm (AChar x) = CEChar (read x)
transformAtomexp cm (AExpr x) = transformExpr cm x
transformAtomexp cm (AVar x) = CEVar x
transformAtomexp cm (ACon x) = CECon t a where
    (t, a) = aLookup cm x (error $ "undefined constructor: " ++ x)

transformDecls :: ConstrMap -> Decls String -> [(String, CExpr String)]
transformDecls cm = map (transformDecl cm)
transformDecl :: ConstrMap -> ValDef String -> (String, CExpr String)
transformDecl cm ((name, args), x) = (name, CELambda args (transformExpr cm x))

transformIf :: ConstrMap -> Branches String -> CExpr String
transformIf cm [] = CEVar "undefined"
transformIf cm ((l, body) : xs) = CEApp (CEApp (CEApp (CEVar "if") (transformExpr cm l)) (transformExpr cm body)) (transformIf cm xs)

transformAlters :: ConstrMap -> [Alter String] -> [CAlter String]
transformAlters cm = map (transformAlter cm)
transformAlter :: ConstrMap -> Alter String -> CAlter String
transformAlter cm (PCon name args, body) = (fst $ aLookup cm name (error $ "Failed to find definition for constructor " ++ name), args, (transformExpr cm body))

transformValdef :: ConstrMap -> TopDecl String -> CScDefn String
transformValdef cm (Valdef ((name, args), body)) = (name, args, (transformExpr cm body))

transformProgram_ :: ConstrMap -> Program String -> CProgram String
transformProgram_ cm = map (transformValdef cm)

transformProgram :: Program String -> CProgram String
transformProgram x = transformProgram_ cm rest where
    (cm, rest) = collect x 