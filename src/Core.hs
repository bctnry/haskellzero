module Core (
    CExpr(..)
,   CName
,   CoreExpr
,   CAlter
,   CoreAlt
,   CProgram
,   CoreProgram
,   CScDefn
,   CoreScDefn
,   CIsRec
,   bindersOf
,   rhssOf
,   recursive
,   nonRecursive
,   isAtomicExpr
) where

data CExpr a
    =   CEVar CName
    |   CEInt Int
    |   CEFloat Double
    |   CEChar Char
    |   CECon Int Int
    |   CEApp (CExpr a) (CExpr a)
    |   CELet CIsRec [(a, CExpr a)] (CExpr a)
    |   CECase (CExpr a) [CAlter a]
    |   CELambda [a] (CExpr a)
    |   CEPrimitive CName    -- !
    deriving (Show)
type CName = String
type CoreExpr = CExpr CName

-- these definitions are equivalent with those in SPJ91.
bindersOf :: [(a, b)] -> [a]
bindersOf = map fst
rhssOf :: [(a, b)] -> [b]
rhssOf = map snd

type CAlter a = (Int, [a], CExpr a)
type CoreAlt = CAlter CName

isAtomicExpr :: CExpr a -> Bool
isAtomicExpr x = case x of {
    CEVar _ -> True;
    CEInt _ -> True;
    CEFloat _ -> True;
    CEChar _ -> True;
    CEPrimitive _ -> True;
    _ -> False
}

type CProgram a = [CScDefn a]
type CoreProgram = CProgram CName
type CScDefn a = (CName, [a], CExpr a)
type CoreScDefn = CScDefn CName

type CIsRec = Bool
recursive, nonRecursive :: CIsRec
recursive = True
nonRecursive = False
